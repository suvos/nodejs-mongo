const express = require('express');

const router = express.Router();
const tourController = require('../controller/api.controller');
const tourMiddleware = require('../middleware/tourMiddleware');

router.param('tour_id', tourController.checkId);
router.route('/tour/login')
  .post(tourController.Login);
router.route('/tour')
  .get(tourMiddleware.verifyToken, tourController.getTours)
  .post(tourMiddleware.verifyToken, tourController.postTours);

router.route('/tour/:tour_id')
  .get(tourController.getTour, tourMiddleware.verifyToken);

router.route('/tourpatch/:tour_id')
  .patch(tourController.patchTours, tourMiddleware.verifyToken);

router.route('/tourdel/:tour_id')
  .delete(tourController.deleteTours, tourMiddleware.verifyToken);

module.exports = router;
