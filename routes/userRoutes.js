const express = require('express');

const router = express.Router();

const usersController = require('../controller/users.controller');

router.route('/users')
  .get(usersController.getUsers)
  .post(usersController.postUsers);

router.route('/users/:id')
  .put(usersController.updateUsers)
  .delete(usersController.deleteUsers);

module.exports = router;
