/* eslint-disable prefer-destructuring */
const token_key = process.env.TOKEN_KEY;
const jwt = require('jsonwebtoken');

const verifyToken = (req, res, next) => {
  let token = req.headers.authorization;
  if (!token) res.status(400).send('Authorization error');

  try {
    token = token.split(' ')[1];
    if (!token || token === null) throw 'Invalid';
    const verify = jwt.verify(token, token_key);
    if (!verify) throw 'Invalid';
    else {
      next();
    }
  } catch (err) {
    res.status(400).send('Invalid Token!!');
  }
};

exports.verifyToken = verifyToken;
