const dotenv = require('dotenv');
const mongoose = require('mongoose');

console.log(process.env.NODE_ENV);
if (process.env.NODE_ENV === 'production') {
  dotenv.config({ path: './prod.env' });
} else {
  dotenv.config({ path: './dev.env' });
}
const DB = process.env.DATABASE.replace('<PASSWORD>', process.env.DATABASE_PASSWORD);
console.log(DB);
mongoose.connect(DB, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
}).then((con) => {
  console.log('Database Connection successful');
}).catch((err) => console.error(err));

const app = require('./app');

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`App is running on port ${port}`);
});
