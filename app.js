/* eslint-disable no-multiple-empty-lines */

const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const morgan = require('morgan');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.raw({ type: 'text/plain' }));

app.use(morgan('dev'));
console.log(process.env.NODE_ENV);
const routertour = require('./routes/tourRoutes');
const routeruser = require('./routes/userRoutes');

app.use((req, res, next) => {
  console.log('Hello calling from middleware 🏰');
  next();
});
app.use('/api/v1', routertour);
app.use('/api/v1', routeruser);
module.exports = app;
