const mongoose = require('mongoose');

const tourSchema = new mongoose.Schema({
  _id: String,
  name: {
    type: String,
    required: [true, 'Should have a name here . '],
    unique: true,
  },
  rating: {
    type: Number,
    default: 4.5,
  },
  price: {
    type: Number,
    required: [true, 'Should have a price'],
  },
}, { _id: false });

const Tour = mongoose.model('Tour', tourSchema);// Create model

module.exports = Tour;
