const mongoose = require('mongoose');

const loginSchema = new mongoose.Schema({
  _id: String,
  username: {
    type: String,
    required: [true, ' Username is required '],
    unique: [true, 'Name should be unique'],
  },
  password: {
    type: String,
  },
}, { _id: true });

const Users = mongoose.model('Users', loginSchema);

module.exports = Users;
