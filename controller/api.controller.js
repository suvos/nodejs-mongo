const fs = require('fs');

const token_key = process.env.TOKEN_KEY;
const jwt = require('jsonwebtoken');

const tours = JSON.parse(
  fs.readFileSync(`${__dirname}/../dev-data/data/tours.json`)
);
const Tours = require('../models/tourModuller');
const Users = require('../models/login-tour.model');

exports.Login = async (req, res, next) => {
  try {
    req.body._id = 'login-'.concat(new Date().getTime().toString());
    let { body } = req;
    if (req.headers['content-type'] === 'text/plain') {
      const buff = Buffer.from(body, 'utf8');
      const converted = buff.toString();
      try {
        body = JSON.parse(converted);
      } catch (err) {
        res.status(400).json({
          status: 'failed',
          message: 'Invalid file',
        });
      }
    }
    if (!body || !body.username || !body.password)res.status(400).json({ message: 'Error' });
    const logger = {
      username: body.username,
      password: body.password,
      _id: body._id,
    };
    const formData = await Users.create(logger);
    const token = jwt.sign(logger, token_key);
    console.log(formData);
    const bearer = `Bearer ${token.trim()}`;
    res.status(200).json({
      message: 'success',
      token: bearer,
      data: formData,
    });
  } catch (err) {
    res.status(500).json({
      status: 'Failed',
      message: err,
    });
  }
};

exports.checkId = (req, res, next, val) => {
  console.log(`The id is ${val}`);
  if (req.params.tour_id * 1 > tours.length) {
    return res.status(404).json({
      status: 'fail',
      startTime: Math.floor(new Date(req._startTime)),
      message: 'Invalid',
    });
  }
  next();
};

exports.getTours = async (req, res) => {
  try {
    const tours = await Tours.find();

    return res.status(200).json({
      status: 'success',
      startTime: Math.floor(new Date(req._startTime)),
      count: tours.length,
      data: tours,
    });
  } catch (err) {
    return res.status(400).json({
      status: 'failed',
      message: err,
    });
  }
};

exports.getTour = async (req, res) => {
  try {
    const tour = await Tours.findById(req.params.tour_id);
    // const tour=await Tours.findOne({ _id: req.params.tour_id});
    return res.status(200).json({
      status: 'success',
      startTime: Math.floor(new Date(req._startTime)),
      data: tour,
    });
  } catch (err) {
    return res.status(400).json({
      status: 'failed',
      message: err,
    });
  }
};

exports.postTours = async (req, res) => {
  try {
    req.body._id = 'tours-'.concat(new Date().getTime().toString());
    const newTour = await Tours.create(req.body);
    return res.status(201).json({
      status: 'Success',
      startTime: Math.floor(new Date(req._startTime)),
      data: {
        tour: newTour,
      },
    });
  } catch (err) {
    return res.status(400).json({
      status: 'failed',
      message: err,
    });
  }
};

exports.patchTours = async (req, res) => {
  try {
    // const tour= await Tours.findByIdAndUpdate(req.params.tour_id,req.body,{
    //     new:true,
    //     runValidators:true
    // });
    const tour = await Tours.updateOne({ _id: req.params.tour_id }, req.body, {
      new: true,
      runValidators: true,
    });

    return res.status(200).json({
      status: 'Success',
      startTime: Math.floor(new Date(req._startTime)),
      data: {
        tour,
      },
    });
  } catch (err) {
    return res.status(400).json({
      status: 'failed',
      message: err,
    });
  }
};

exports.deleteTours = async (req, res) => {
  try {
    const deltour = await Tours.deleteOne({ _id: req.params.tour_id }, {
      new: true,
      runValidators: true,
    });
    return res.status(200).json({
      status: 'Success',
      startTime: Math.floor(new Date(req._startTime)),
      data: {
        tour: deltour,
      },
    });
  } catch (err) {
    return res.status(400).json({
      status: 'failed',
      message: err,
    });
  }
};
